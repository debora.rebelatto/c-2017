#include <stdio.h>
#include <stdlib.h>

int main()
{
    //Typecasting = Temporarily change the data type of a variable
    //turning floats into ints, ints into floats and so on

    float avgProfit;
    int priceOfPupking = 10;
    int sales = 59;
    int daysWorked = 7;
    //To use that, make specific the data type before
    avgProfit = ( (float)priceOfPupking * (float)sales) / (float)daysWorked;
    printf("Average daily profit: %.2f$", avgProfit);


    return 0;
}

#include <stdio.h>
#include <stdlib.h>

struct  user{
    int userID;
    char firstName[25];
    char lastName[25];
    int age;
    float weight;
};


int main()
{
    struct user Bucky;
    struct user Gabriel;

    Bucky.userID = 1;
    Gabriel.userID = 2;
    puts("Enter the first name of user 1");
    gets(Bucky.firstName);
    puts("Enter the first name of user 2");
    gets(Gabriel.firstName);

    printf("User 1 ID is %d\n", Bucky.userID);
    printf("User 2 first name is %s\n", Gabriel.firstName);

}
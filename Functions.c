#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE * fpointer;
    // w is for write
    // r is for read
    // a add more crap
    // open to write, then for reading
    // feseek allow you do hop to random
    //        points in the file
    // SEEK_SET start at the beggining
    // SEEK_END start at the end
    
    fpointer = fopen("bacon.txt", "w+");

    fputs("I don't know this\n", fpointer);

    fseek(fpointer, 8, SEEK_SET);

    fputs("anything about", fpointer);

    fseek(fpointer, -5, SEEK_END);

    fputs("at all", fpointer);
    
    fclose(fpointer);
}

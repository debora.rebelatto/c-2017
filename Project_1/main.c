#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("Debourar\n");
// \n is to new line
    printf("follow me\t");
    printf("Cats\n");
// \t is for distance
    printf("%s is the best %s ever\n", "Bucky", "follow");
//%s is only for text
    printf("I ate %d french fries last night\n", 9);
//numbers don't go in between quotation marks
    printf("Pi is %.2f\n", 3.1415926535);
    printf("Pi is %.4f\n", 3.1415926535);
    printf("Pi is %f\n", 3.1415926535);
// %f is only used for numbers with decimal place, that is a float
// The .number is to show the float, but only to 2 decimal places
// That is a variable:
    int age;
    int currentYear;
    int birthYear;
// Use int to define
    currentYear = 2017;
    birthYear = 1988;
    age = currentYear - birthYear;
    printf("Debourar is %d years old\n", age);
//This is an array:
    char name[9] = "Debourar";
    name[2] = 'z';
    printf("My name is %s \n", name);
    char food[] = "tuna";
    printf("The best food is %s \n", food);
    strcpy(food, "bacon");
    printf("The best food is %s \n", food);
    return 0;
}
